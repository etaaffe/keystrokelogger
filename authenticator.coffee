@PAUSE_LENGTH = 1000 # millis 
@SNAPSHOT_LENGTH = 100
@BAD_KEYS = [18] # [alt]
@BASE_URL = "https://fast-mountain-3727.herokuapp.com"

@keys = []
@keyPressHash = {}
@outputTextArea 
@timeoutVar
@user_id
@mode = "train"
@cacheData = ""
@questionDescription
@pauseAmount
@sessionId
@oldVal = ""
@snapshot = ""

@setQuestionDescription = (desc) ->
	@questionDescription = desc

@setSessionId = (id) -> 
  @sessionId = id


@setUserId = (id) ->
	@user_id = id

@setMode = (mode) ->
	@mode = mode

@setOutputTextArea = (area) ->
	@outputTextArea = area

@keyPress = (e) ->
	if @BAD_KEYS.indexOf(e.keyCode) > -1
		console.log "BAD KEY, dont record"
		return
	window.clearTimeout(@timeoutVar)
	if !(e.keyCode in @keyPressHash)
		keys.push(e.keyCode)
		@keyPressHash[e.keyCode] = getTimeInMillis()

@keyRelease = (e,area) ->
	caretPosition = area.selectionStart
	snapshotStart = 0

	if (caretPosition >= @SNAPSHOT_LENGTH)
		snapshotStart = caretPosition - 100
	else
		snapshotStart = 0
	
	console.log "startSnapshot: " + snapshotStart + ", endSnapshot:  " + caretPosition
	textLength = area.value.length
	@snapshot = area.value.substr(snapshotStart,caretPosition)
	if @BAD_KEYS.indexOf(e.keyCode) > -1
		console.log "BAD KEY, dont record"
		return
	window.clearTimeout(@timeoutVar)
	@cacheData += @keyPressHash[e.keyCode] + "," + getTimeInMillis() + "," + e.keyCode + "," + textLength + "\n"
	@outputMsg(@keyPressHash[e.keyCode] + "," + getTimeInMillis() + "," + e.keyCode + "," + textLength)
	delete @keyPressHash[e.keyCode]

	if (Object.keys(@keyPressHash).length == 0)
		@timeoutVar = window.setTimeout(pauseEvent, PAUSE_LENGTH)

@editOccured = (e,selectionStart,selectionEnd,val) ->
	console.log diff(@oldVal, val)
	console.log "selectionStart: " + selectionStart + " , selectionEnd: " + selectionEnd
	@oldVal = val
	

getTimeInMillis = ->
	return new Date().getTime()

@pauseEvent = ->
	if !(Object.keys(@keyPressHash).length == 0)
		@keyPressHash = {}
	else
		@outputMsg("============PAUSE============")
		console.log(@cacheData)
		if (@user_id == "")
			@outputMsg("You first need to select a user id before anything is saved to the database")
		sendData(user_id, mode, @cacheData)
		@cacheData = ""

@sendData = (userIdData, modeData, dataData) ->
	userData = {data:{user_id: userIdData, mode: modeData, keystroke_data: dataData, session_id: @sessionId, question_description: @questionDescription, snapshot: @snapshot}};
	console.log userData
	try
		$.ajax({
	      type: "POST"
	      crossOrigin: true
	      crossDomain:true
	      data: userData
	      xhrFields: { withCredentials: true}
	      dataType: "jsonp"
	      header:{"Access-Control-Allow-Origin":@BASE_URL}
	      url: @BASE_URL + "/burst"
	      success:(data, textStatus) ->
	      	console.log "SUCCESS"
	      	console.log data
	      error:(xhr, textStatus, errorThrown) ->
	        console.log "FAILURE"
	    })
	catch error
    console.error error
  finally
    return false


parseResponse = (json) ->
	console.log json


@setHeader = (xhr) ->
	xhr.setRequestHeader('Access-Control-Allow-Headers','*')
	console.log xhr.getResponseHeader()

@keystrokeEvent = (e) ->
  n = new Date().getTime()
  return "(keycode, millis): (" + e.keyCode + ", " + n + ")"

@outputMsg = (msg) ->
	console.log msg

diff = (oldString, newString) ->
	return oldString - newString

$ ->
	setOutputTextArea $('#authenticatorKeypress')
	setUserId(5678)
	setSessionId(1234)
	setQuestionDescription("Example Webpage")

	$('textarea[data-authenticator]').keydown (e) ->
		type = $(this).data("authenticator")
		setMode(type)
		keyPress(e)

	$('textarea[data-authenticator]').keyup (e) ->
		type = $(this).data("authenticator")
		setMode(type)
		text = this.value
		keyRelease(e,this)
		editOccured(e,this.selectionStart,this.selectionEnd,this.value)

	$('textarea[data-authenticator]').change (e) ->
		editOccured(e,this.selectionStart,this.selectionEnd,this.value)

	$('#user_id').change (e) ->
		setUserId $('#user_id').val()

  
  

# Now I want to separate keystrokes into bursts using pauses. 
# The pause length (p) will initially be 1 second but can be experimented with
# Criteria for a pause:
# - No keys are currently being pressed
# - No new keys are pressed for p seconds