var diff, getTimeInMillis, parseResponse,
  indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; };

this.PAUSE_LENGTH = 1000;

this.SNAPSHOT_LENGTH = 100;

this.BAD_KEYS = [18];

this.BASE_URL = "https://fast-mountain-3727.herokuapp.com";

this.keys = [];

this.keyPressHash = {};

this.outputTextArea;

this.timeoutVar;

this.user_id;

this.mode = "train";

this.cacheData = "";

this.questionDescription;

this.pauseAmount;

this.sessionId;

this.oldVal = "";

this.snapshot = "";

this.setQuestionDescription = function(desc) {
  return this.questionDescription = desc;
};

this.setSessionId = function(id) {
  return this.sessionId = id;
};

this.setUserId = function(id) {
  return this.user_id = id;
};

this.setMode = function(mode) {
  return this.mode = mode;
};

this.setOutputTextArea = function(area) {
  return this.outputTextArea = area;
};

this.keyPress = function(e) {
  var ref;
  if (this.BAD_KEYS.indexOf(e.keyCode) > -1) {
    console.log("BAD KEY, dont record");
    return;
  }
  window.clearTimeout(this.timeoutVar);
  if (!(ref = e.keyCode, indexOf.call(this.keyPressHash, ref) >= 0)) {
    keys.push(e.keyCode);
    return this.keyPressHash[e.keyCode] = getTimeInMillis();
  }
};

this.keyRelease = function(e, area) {
  var caretPosition, snapshotStart, textLength;
  caretPosition = area.selectionStart;
  snapshotStart = 0;
  if (caretPosition >= this.SNAPSHOT_LENGTH) {
    snapshotStart = caretPosition - 100;
  } else {
    snapshotStart = 0;
  }
  console.log("startSnapshot: " + snapshotStart + ", endSnapshot:  " + caretPosition);
  textLength = area.value.length;
  this.snapshot = area.value.substr(snapshotStart, caretPosition);
  if (this.BAD_KEYS.indexOf(e.keyCode) > -1) {
    console.log("BAD KEY, dont record");
    return;
  }
  window.clearTimeout(this.timeoutVar);
  this.cacheData += this.keyPressHash[e.keyCode] + "," + getTimeInMillis() + "," + e.keyCode + "," + textLength + "\n";
  this.outputMsg(this.keyPressHash[e.keyCode] + "," + getTimeInMillis() + "," + e.keyCode + "," + textLength);
  delete this.keyPressHash[e.keyCode];
  if (Object.keys(this.keyPressHash).length === 0) {
    return this.timeoutVar = window.setTimeout(pauseEvent, PAUSE_LENGTH);
  }
};

this.editOccured = function(e, selectionStart, selectionEnd, val) {
  console.log(diff(this.oldVal, val));
  console.log("selectionStart: " + selectionStart + " , selectionEnd: " + selectionEnd);
  return this.oldVal = val;
};

getTimeInMillis = function() {
  return new Date().getTime();
};

this.pauseEvent = function() {
  if (!(Object.keys(this.keyPressHash).length === 0)) {
    return this.keyPressHash = {};
  } else {
    this.outputMsg("============PAUSE============");
    console.log(this.cacheData);
    if (this.user_id === "") {
      this.outputMsg("You first need to select a user id before anything is saved to the database");
    }
    sendData(user_id, mode, this.cacheData);
    return this.cacheData = "";
  }
};

this.sendData = function(userIdData, modeData, dataData) {
  var error, error1, userData;
  userData = {
    data: {
      user_id: userIdData,
      mode: modeData,
      keystroke_data: dataData,
      session_id: this.sessionId,
      question_description: this.questionDescription,
      snapshot: this.snapshot
    }
  };
  console.log(userData);
  try {
    return $.ajax({
      type: "POST",
      crossOrigin: true,
      crossDomain: true,
      data: userData,
      xhrFields: {
        withCredentials: true
      },
      dataType: "jsonp",
      header: {
        "Access-Control-Allow-Origin": this.BASE_URL
      },
      url: this.BASE_URL + "/burst",
      success: function(data, textStatus) {
        console.log("SUCCESS");
        return console.log(data);
      },
      error: function(xhr, textStatus, errorThrown) {
        return console.log("FAILURE");
      }
    });
  } catch (error1) {
    error = error1;
    return console.error(error);
  } finally {
    return false;
  }
};

parseResponse = function(json) {
  return console.log(json);
};

this.setHeader = function(xhr) {
  xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
  return console.log(xhr.getResponseHeader());
};

this.keystrokeEvent = function(e) {
  var n;
  n = new Date().getTime();
  return "(keycode, millis): (" + e.keyCode + ", " + n + ")";
};

this.outputMsg = function(msg) {
  return console.log(msg);
};

diff = function(oldString, newString) {
  return oldString - newString;
};

$(function() {
  setOutputTextArea($('#authenticatorKeypress'));
  setUserId(5678);
  setSessionId(1234);
  setQuestionDescription("Example Webpage");
  $('textarea[data-authenticator]').keydown(function(e) {
    var type;
    type = $(this).data("authenticator");
    setMode(type);
    return keyPress(e);
  });
  $('textarea[data-authenticator]').keyup(function(e) {
    var text, type;
    type = $(this).data("authenticator");
    setMode(type);
    text = this.value;
    keyRelease(e, this);
    return editOccured(e, this.selectionStart, this.selectionEnd, this.value);
  });
  $('textarea[data-authenticator]').change(function(e) {
    return editOccured(e, this.selectionStart, this.selectionEnd, this.value);
  });
  return $('#user_id').change(function(e) {
    return setUserId($('#user_id').val());
  });
});