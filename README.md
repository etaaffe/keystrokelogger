# README #

### KeystrokeLogger ###

* This repository contains the necessary javascript assets required to capture the keystroke dynamics of a user typing in a text field.
* Version 1

### How do I get set up? ###

* The files are written in coffeescript, so a coffeescript compile is neccessary. If your system doesnt support coffeescript, instructions on how to convert to javascript can be found [here](http://stackoverflow.com/questions/10441452/convert-a-coffeescript-project-to-javascript-without-minification).
* The authenticator.coffee scipt should be included on the necessary page.
* The text field which the user will be typing in needs to be tagged with either `"data-authenticator":"train"` or `"data-authenticator":"test"` depending on if the user data should be tested or added to the training set.
* Once the text field is tagged accordingly, the script will run in the background sending packets to the KeystrokeServer systematically.

### Example Implementation

* An example implementation of the KeystrokeLogger service can be found [here](https://fast-mountain-3727.herokuapp.com/). You can view the source code to examine the implementation and view the javascript console output to view whats happening in the background.

### Contact ###

* Eamon Taaffe
* eamontaaffe@gmail.com